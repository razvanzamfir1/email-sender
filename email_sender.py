import smtplib   # ne ajuta sa creem un server de fiecare data cand trimitem email care comunica limba email-ului. Este ,protocolul unic' al email-urilor. smtp (simple mail transfer protocol)
from email.message import EmailMessage   # este un modul built-inPython
from string import Template   # cu template putem substituie variabilele inauntrul textului foflosin semnul $
from pathlib import Path  # acest modul ne lasa sa accesam index.html file

html = Template(Path('index.html').read_text()) # acestv Path ne da accesul la index.html si cu metoda read_text() putem citi fisierul ca un string, ca un text.
                                                # includem stringul intr-un template ca sa avem un Template oject. Daca avem template oject putem sa folosim methoda substitute()
#1. crearea obiectului email

email = EmailMessage()  # acesta este obiectul email creat (from, to, subject, content)
email['from'] = 'Zamfir Razvan'
email['to'] = 'razvanzamfir1@yahoo.com'
email['subject'] = 'You are the best!'
#
email.set_content(html.substitute({'name' :'TinTin'}), 'html') # aici putem pune orice fel de continut vrem (text,HTML, images)

# 2. folosirea librariei smtplib pentru logare la adresa de g-mail de unde vom trimite mail

with smtplib.SMTP(host='smtp.gmail.com', port=587) as smtp:
    smtp.ehlo() # hei aici este serverul, am sa iti dau hello msg
    smtp.starttls() # criptarea
    email_adress = 'razvanzamfir1@gmail.com'
    email_password = 'riivmzuhhtfidymm'  # se genereaza o parola pentru aplicatie din contul google de gmail
    smtp.login(email_adress, email_password) # acum ne putem loga la server
    smtp.send_message(email)
    print('all good boss!')

# with smtplib.SMTP_SSL(host='smtp.yahoo.com', port=587) as connection:
#     email_adress = 'razvanzamfir1@yahoo.com'
#     email_password = 'Abcd12345'
#     connection.login(email_adress, email_password)
#     connection.sendmail(from_addr=email_adress , to_addrs='razvanzamfir1@gmail.com')
#     msg= ("subject: You won 1.000.000 dolars\n\n this is a message")
#     print('all good boss!')

# utilizare trimitere email:
# de facut alicatii
# cand cineva iti cumpara un produs de pe site si vrei sa ii trimiti un mesaj de multumire
# sau sa iti setezi ca in fiecare dimineata sa iti trimiti un email in care sa iti reaminteasca ceva.
